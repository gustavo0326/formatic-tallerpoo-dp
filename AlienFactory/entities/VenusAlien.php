<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VenusAlien
 *
 * @author pabhoz
 */
class VenusAlien extends GoodAlien{
    public function __construct($nombre, $edad, $especie) {
        parent::__construct($nombre, $edad, $especie, "Venus");
    }
    
    public function interact() {
        $text = parent::interact();
        return str_replace(",", ", hmmm,", $text);
    }
    
    public function whoIAm() {
        $text = parent::whoIAm();
        return str_replace(",", ", hmmm,", $text);
    }
}
