<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alien
 *
 * @author pabhoz
 */
abstract class Alien {
    
    private $nombre, $edad, $especie, $planeta, $moral;
    const COMUNICACION = "Telepaticamente";
    
    function __construct($nombre, $edad, $especie, $planeta, $moral = "neutral") {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->especie = $especie;
        $this->planeta = $planeta;
        $this->moral = $moral;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function getEspecie() {
        return $this->especie;
    }

    function getPlaneta() {
        return $this->planeta;
    }

    function getMoral() {
        return $this->moral;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setEspecie($especie) {
        $this->especie = $especie;
    }

    function setPlaneta($planeta) {
        $this->planeta = $planeta;
    }

    function setMoral($moral) {
        $this->moral = $moral;
    }

    abstract public function interact();
    
    public function whoIAm(){
        return Alien::COMUNICACION." dice: Mi nombre es ".$this->getNombre().", "
                . "del planeta ".$this->getPlaneta().", soy un ".$this->getEspecie()
                ." y soy ".$this->getMoral();
    }
}
